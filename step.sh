#!/bin/bash
set -e

THIS_SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#=======================================
# Functions
#=======================================

RESTORE='\033[0m'
RED='\033[00;31m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
GREEN='\033[00;32m'

function color_echo {
	color=$1
	msg=$2
	echo -e "${color}${msg}${RESTORE}"
}

function echo_fail {
	msg=$1
	echo
	color_echo "${RED}" "${msg}"
	exit 1
}

function echo_warn {
	msg=$1
	color_echo "${YELLOW}" "${msg}"
}

function echo_info {
	msg=$1
	echo
	color_echo "${BLUE}" "${msg}"
}

function echo_details {
	msg=$1
	echo "  ${msg}"
}

function echo_done {
	msg=$1
	color_echo "${GREEN}" "  ${msg}"
}

function validate_required_input {
	key=$1
	value=$2
	if [ -z "${value}" ] ; then
		echo_fail "[!] Missing required input: ${key}"
	fi
}

function validate_required_input_with_options {
	key=$1
	value=$2
	options=$3

	validate_required_input "${key}" "${value}"

	found="0"
	for option in "${options[@]}" ; do
		if [ "${option}" == "${value}" ] ; then
			found="1"
		fi
	done

	if [ "${found}" == "0" ] ; then
		echo_fail "Invalid input: (${key}) value: (${value}), valid options: ($( IFS=$", "; echo "${options[*]}" ))"
	fi
}



python3 -m venv env-s3-upload
source env-s3-upload/bin/activate
python3 --version
pip3 --version
pip3 install -e $THIS_SCRIPT_DIR

#=======================================
# Main
#=======================================

#
# Validate parameters
echo_info "Configs:"
if [[ -n "$access_key_id" ]] ; then
	echo_details "* access_key_id: ***"
else
	echo_details "* access_key_id: [EMPTY]"
fi
if [[ -n "$secret_access_key" ]] ; then
	echo_details "* secret_access_key: ***"
else
	echo_details "* secret_access_key: [EMPTY]"
fi
echo_details "* upload_bucket: $upload_bucket"
echo_details "* upload_local_path: $upload_local_path"
echo_details "* acl_control: $acl_control"
echo_details "* set_acl_only_on_changed_objets: $set_acl_only_on_changed_objets"
echo_details "* aws_region: $aws_region"
echo_details "* deploy_path: $deploy_path"
echo_details "* debug: $debug"
echo

validate_required_input "access_key_id" $access_key_id
validate_required_input "secret_access_key" $secret_access_key
validate_required_input "upload_bucket" $upload_bucket
validate_required_input "upload_local_path" $upload_local_path

if [[ "$aws_region" != "" ]] ; then
	echo_details "AWS region (${aws_region}) specified!"
	export AWS_DEFAULT_REGION="${aws_region}"
fi

export AWS_ACCESS_KEY_ID="${access_key_id}"
export AWS_SECRET_ACCESS_KEY="${secret_access_key}"

prefix=""
if [ $deploy_path != "." ]
then
	prefix="--prefix $deploy_path"
fi

if [ $debug == "yes" ]
then
	prefix="--debug $prefix"
fi

which s3-site-cache-optimizer

echo_details "command: s3-site-cache-optimizer $upload_local_path $upload_bucket --skip-fingerprinting $prefix"
s3-site-cache-optimizer $upload_local_path $upload_bucket --skip-fingerprinting $prefix

echo_done "Success"
if [[ -n ${AWS_DEFAULT_REGION} ]] ; then
  echo_details "AWS Region: ${aws_region}"
fi
echo_details "Base URL: http://${upload_bucket}.s3.amazonaws.com/"
