import argparse
import os
import logging
import re
import gzip
import importlib.metadata
from hashlib import sha256
from shutil import copyfile, move, rmtree
from fnmatch import fnmatch
from tempfile import mkdtemp, mkstemp
from urllib.parse import urlparse, urljoin
import mimetypes

import boto3
from botocore.exceptions import ClientError

__author__ = "Ruben Van den Bossche"
__email__ = "ruben@appstrakt.com"
__copyright__ = "Copyright 2015, Appstrakt BVBA"
__license__ = "MIT"

logger = logging.getLogger('s3-site-cache-optimizer')


def calculate_fingerprint(fname):
    '''Calculate a hash from a file name.'''
    hasher = sha256()
    blocksize = 65536

    with open(fname, 'rb') as f:
        buf = f.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = f.read(blocksize)
    filehash = hasher.hexdigest()
    logger.debug("Fingerprint of {0} is {1}".format(fname, filehash))
    return filehash


def convert_filename(filename, filehash):
    '''Convert filename to include file hash'''
    ftup = os.path.splitext(filename)
    return ftup[0] + '.' + filehash + ftup[1]

def by_chunk(items, chunk_size=1000):
    """
    Separate iterable objects by chunks

    For example:
    >>> by_chunk([1, 2, 3, 4, 5], chunk_size=2)
    >>> [[1, 2], [3, 4], [5]]

    Parameters
    ----------
    chunk_size: int
    items: Iterable

    Returns
    -------
    List
    """
    bucket = []
    for item in items:
        if len(bucket) >= chunk_size:
            yield bucket
            bucket = []
        bucket.append(item)
    yield bucket

class OptimizerError(Exception):
    '''Error thrown by Optimizer class.'''
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return str(self.msg)


class Optimizer(object):
    '''Optimizer class: Optimize a static website for hosting in S3.'''

    def __init__(self, source_dir, destination_bucket, exclude=[], skip_assets=[], output_dir=None,
                 aws_access_key_id=None, aws_secret_access_key=None, skip_s3_upload=False,
                 region=None, domains=[], prefix=None, gzip=False, skip_fingerprinting=False):
        '''Initialize Optimizer'''

        logger.info('Initialize Optimizer class')

        if not os.path.isdir(source_dir):
            raise OptimizerError("{0} is not a valid path".format(source_dir))

        if not os.access(source_dir, os.R_OK):
            raise OptimizerError("{0} is not a readable dir".format(source_dir))

        if output_dir is not None:
            output_dir_is_temp = False
            if not os.path.isdir(output_dir):
                try:
                    os.makedirs(output_dir)
                except os.error:
                    raise OptimizerError("{0} is not a directory and cannot be created".format(output_dir))

            if not os.access(output_dir, os.W_OK):
                raise OptimizerError("{0} is not a writable dir".format(output_dir))
        else:
            output_dir_is_temp = True
            output_dir = mkdtemp()

        self._assets_ext = ['.css', '.svg', '.ttf', '.woff', '.woff2', '.otf', '.eot', '.png',
                            '.jpg', '.jpeg', '.gif', '.js', '.mp4', '.webm', '.webp']
        self._rewriteables_ext = ['.html', '.htm', '.js', '.css', '.xml', '.json']
        self._gzip_ext = ['.html', '.htm', '.css', '.js', '.svg', '.xml', '.json']
        self._content_types = {'.wasm': 'application/wasm'}

        self._source_dir = source_dir
        self._output_dir = output_dir
        self._output_dir_is_temp = output_dir_is_temp
        self._destination_bucket = destination_bucket
        self._subdirs = []
        self._files = []
        self._assets_map = {}
        self._rewritables = []
        self._exclude = exclude
        self._skip_assets = skip_assets
        self._domains = domains
        self._prefix = prefix
        self._gzip = gzip
        self._skip_s3_upload = skip_s3_upload
        self._skip_fingerprinting = skip_fingerprinting

        if not self._skip_s3_upload:
            try:
                session = boto3.Session(
                    aws_access_key_id=aws_access_key_id,
                    aws_secret_access_key=aws_secret_access_key,
                    region_name=region
                )
                self._s3 = session.resource('s3')
                self._bucket = self._s3.Bucket(self._destination_bucket)
                self._bucket.load()  # Raise an exception if the bucket doesn't exist
            except ClientError as e:
                raise OptimizerError("Cannot connect to S3: " + str(e))

        logger.debug('Optimizer class initialized')

    def __del__(self):
        if self._output_dir_is_temp:
            try:
                logger.debug('Deleting temporary directory')
                rmtree(self._output_dir)
            except OSError:
                raise OptimizerError("Can't delete temporary directory.")

    def _index_source_dir(self):
        '''
        Index all files and directories under the source directory
        '''

        logger.info('Indexing source dir')
        for dirpath, dirnames, fnames in os.walk(self._source_dir):

            for d in dirnames:
                relpath = os.path.relpath(
                    os.path.join(dirpath, d), self._source_dir)
                if True in [fnmatch(relpath, exclude) for exclude in self._exclude]:
                    continue

                logger.debug("Found subdir {0}".format(relpath))
                self._subdirs.append(relpath)

            for f in fnames:
                relpath = os.path.relpath(
                    os.path.join(dirpath, f), self._source_dir)
                if True in [fnmatch(relpath, exclude) for exclude in self._exclude]:
                    continue

                self._files.append(relpath)

                ext = os.path.splitext(f)[1]
                if ext in self._assets_ext:
                    if any(fnmatch(relpath, skip) for skip in self._skip_assets):
                        logger.debug("Skipping asset {0}".format(f))
                    else:
                        logger.debug("Found asset {0}".format(f))
                        self._assets_map[relpath] = {
                            'basename': os.path.basename(relpath)}

                if ext in self._rewriteables_ext:
                    logger.debug("Found rewritable {0}".format(f))
                    self._rewritables.append(relpath)

        logger.debug('Finished indexing source dir')

    def _calculate_fingerprints(self):
        '''
        Calculate fingerprints of all assets in the source dir
        '''

        logger.info('Calculating fingerprints')

        for fname in self._assets_map.keys():
            fingerprint = calculate_fingerprint(
                os.path.join(self._source_dir, fname))
            self._assets_map[fname]['new_filename'] = convert_filename(
                fname, fingerprint)

        logger.debug('Finished calculating fingerprints')

    def _write_dirs(self):
        '''
        Write directory structure to output folder
        '''

        logger.info('Writing dirs')
        for reldir in self._subdirs:
            absdir = os.path.join(self._output_dir, reldir)
            if not os.path.isdir(absdir):
                logger.debug("Making dir {0}".format(absdir))
                os.makedirs(absdir)

        logger.debug('Finished writing dirs')

    def _rewrite_file(self, src, dst):
        '''
        rewrite a single file from source to dest
        '''

        with open(src, 'r') as f_src:
            src_reldirpath = os.path.dirname(
                os.path.relpath(src, self._source_dir))

            with open(dst, 'w') as f_dst:
                # replace asset urls line by line
                for line in f_src:
                    for asset in self._assets_map.keys():
                        if self._assets_map[asset]['basename'] in line:
                            url_chars = '''a-z0-9''' + \
                                re.escape('''-_.~!#$&*+,/:;=?@[]''')
                            regex = r'''[''' + url_chars + ''']*''' + \
                                    re.escape(self._assets_map[asset]['basename']) + \
                                    '[' + url_chars + ']*'

                            it = re.finditer(regex, line, re.IGNORECASE)
                            for result in reversed(list(it)):
                                url = result.group()
                                parsed_url = urlparse(url)
                                if parsed_url.netloc and \
                                        parsed_url.netloc not in self._domains:
                                    # leave this url alone, is third party
                                    logger.warning(
                                        "Skipping rewriting url {0}.".format(url))
                                    continue
                                logger.debug(
                                    "Found asset {0} in {1}".format(url, src))
                                normalized_relative_path = os.path.normpath(
                                    os.path.join(src_reldirpath, parsed_url.path)).lstrip('/')

                                if asset == normalized_relative_path:
                                    new_path = '/' + os.path.join(
                                        os.path.dirname(
                                            normalized_relative_path),
                                        os.path.basename(self._assets_map[asset]['new_filename']))

                                    if parsed_url.netloc:
                                        # don't remove domain from absolute urls
                                        new_path = urljoin(url, new_path)

                                    logger.debug(
                                        "Replacing with {0}".format(new_path))

                                    line = line[:result.start()] + \
                                        new_path + line[result.end():]

                    f_dst.write(line)

    def _write_files(self):
        '''
        Write files to output folder, and rewrite file names/content if necessary.
        '''

        logger.info('Writing files')
        assets = set(self._assets_map.keys())
        rewritables = set(self._rewritables)

        # (1) Assets that are not rewritables
        for src_filename in assets - rewritables:
            dst_filename = self._assets_map[src_filename]['new_filename']
            src = os.path.join(self._source_dir, src_filename)
            dest = os.path.join(self._output_dir, dst_filename)

            logger.debug("1. Writing asset {0} to {1}".format(
                src_filename, dst_filename))
            copyfile(src, dest)

        # (2) Assets that are also rewritables
        for src_filename in assets & rewritables:
            # we assume these files don't have mutual references to eachother
            # if they do, we need to build a dependency tree and solve order
            # that way (TODO)

            # make temp file
            tmp_handle, tmp_filename = mkstemp(text=True)

            src = os.path.join(self._source_dir, src_filename)
            logger.debug(
                "2. Rewriting asset {0} to temp file".format(src_filename))
            self._rewrite_file(src, tmp_filename)

            # close temp file and calculate fingerprint
            fingerprint = calculate_fingerprint(tmp_filename)
            dst_filename = convert_filename(src_filename, fingerprint)

            self._assets_map[src_filename]['new_filename'] = dst_filename

            # move temp file to destination
            dest = os.path.join(self._output_dir, dst_filename)

            logger.debug("2. Writing asset {0} to {1}".format(
                src_filename, dst_filename))
            move(tmp_filename, dest)

        # (3) Other rewritables
        for src_filename in rewritables - assets:
            src = os.path.join(self._source_dir, src_filename)
            dest = os.path.join(self._output_dir, src_filename)

            logger.debug("3. Rewriting asset {0}".format(src_filename))
            self._rewrite_file(src, dest)

        # (4) Other files
        for src_filename in set(self._files) - assets - rewritables:
            src = os.path.join(self._source_dir, src_filename)
            dest = os.path.join(self._output_dir, src_filename)

            logger.debug("4. Copying file {0}".format(src_filename))
            copyfile(src, dest)

        logger.debug('Finished writing files')

    def _gzip_files(self):
        '''
        Gzip text files in output folder.
        '''

        logger.info('Gzipping files')

        for dirpath, dirnames, fnames in os.walk(self._output_dir):
            for f in fnames:
                abspath = os.path.join(dirpath, f)

                is_gzippable = os.path.splitext(f)[1] in self._gzip_ext
                if is_gzippable:
                    tmp_handle, tmp_filename = mkstemp()

                    logger.debug('Gzipping {0}'.format(abspath))
                    with open(abspath, 'rb') as f_in:
                        with gzip.open(tmp_filename, 'wb') as f_out:
                            f_out.writelines(f_in)

                    # overwrite existing file
                    move(tmp_filename, abspath)

        logger.debug('Finished gzipping files')

    def _compare_file_names(self, item, item2=None):
        if item.endswith('.html'):
            return 1
        elif item.endswith('service-worker.js') or item.endswith('sw.js'):
            return 1
        else:
            return 0

    def _upload_to_bucket(self):
        '''
        Upload contents of output folder to S3.
        '''
        logger.info('Uploading to bucket')

        try:
            if self._prefix:
                to_be_deleted = [
                    obj.key for obj in self._bucket.objects.filter(Prefix=self._prefix)]
            else:
                to_be_deleted = [
                    obj.key for obj in self._bucket.objects.all()]

            final_paths = []
            abs_paths = {}
            for dirpath, dirnames, fnames in os.walk(self._output_dir):
                for f in fnames:
                    abspath = os.path.join(dirpath, f)
                    relpath = os.path.relpath(abspath, self._output_dir)

                    if self._prefix:
                        relpath = os.path.join(self._prefix, relpath)

                    # Remove this file from the deletion list if it exists
                    if relpath in to_be_deleted:
                        to_be_deleted.remove(relpath)

                    final_paths.append(relpath)
                    abs_paths[relpath] = abspath

            # Sort file names to ensure .html files are uploaded last
            final_paths.sort(key=self._compare_file_names)

            for final_path in final_paths:
                final_abs_path = abs_paths.get(final_path)

                # Check if file should be cached/reuploaded
                file_name = os.path.basename(final_path)
                ext = os.path.splitext(final_path)[1]
                cache_asset = ext in self._assets_ext

                # Don't cache service workers
                if file_name in ['service-worker.js', 'sw.js']:
                    cache_asset = False

                is_gzipped = self._gzip and (ext in self._gzip_ext)

                headers = {}
                if cache_asset:
                    # Set infinite headers
                    headers['CacheControl'] = "public, max-age=604800, s-maxage=604800"
                else:
                    # Set no-cache headers
                    headers['CacheControl'] = "no-cache, max-age=0, s-maxage=0"

                if is_gzipped:
                    headers['ContentEncoding'] = "gzip"

                try:
                    headers['ContentType'] = self._content_types[ext]
                except KeyError:
                    guessed_type, _ = mimetypes.guess_type(final_path)
                    if guessed_type:
                        headers['ContentType'] = guessed_type
                    pass

                logger.info("Uploading file {0} to {1} - cached: {2} - Headers: {3}".format(final_path,
                                                                            self._destination_bucket, cache_asset, headers))
                self._bucket.upload_file(final_abs_path, final_path, ExtraArgs=headers)

            # Remove files not currently uploaded
            if to_be_deleted:
                for items in by_chunk(to_be_deleted, 1000):
                    delete_objects = {'Objects': [{'Key': key} for key in items]}
                    response = self._bucket.delete_objects(Delete=delete_objects)
                    for deleted in response.get('Deleted', []):
                        logger.debug("Deleted key {} from {}".format(deleted['Key'], self._destination_bucket))

        except Exception as e:
            logger.error("Error uploading to S3: " + str(e))
            raise OptimizerError("Error uploading to S3: " + str(e))

        logger.debug('Finished uploading to bucket')

    def optimize(self):
        '''
        Main entry method for the Optimizer object.
        '''

        if self._skip_fingerprinting:
            logger.debug('Running optimize without fingerprinting')
            self._output_dir = self._source_dir
            self._output_dir_is_temp = False
        else:
            logger.debug('Running optimize')

            self._index_source_dir()
            self._calculate_fingerprints()
            self._write_dirs()
            self._write_files()

            if self._gzip:
                self._gzip_files()

        if not self._skip_s3_upload:
            self._upload_to_bucket()

        logger.info('Optimization complete')


def main():
    parser = argparse.ArgumentParser(description="Optimize static websites for S3.")
    parser.add_argument('source_dir', help="Directory containing source files.")
    parser.add_argument('destination_bucket', help="S3 bucket to upload to.")
    parser.add_argument('--exclude', action='append', default=[], help="Files to exclude.")
    parser.add_argument('--skip-assets', action='append', default=[], help="Assets to skip.")
    parser.add_argument('--output-dir', help="Directory for output files.")
    parser.add_argument('--aws-access-key-id', help="AWS Access Key ID.")
    parser.add_argument('--aws-secret-access-key', help="AWS Secret Access Key.")
    parser.add_argument('--skip-s3-upload', action='store_true', help="Skip uploading to S3.")
    parser.add_argument('--region', help="AWS region.")
    parser.add_argument('--domains', action='append', default=[], help="Allowed domains for asset URLs.")
    parser.add_argument('--prefix', help="Prefix for S3 keys.")
    parser.add_argument('--gzip', action='store_true', help="Gzip output files.")
    parser.add_argument('--skip-fingerprinting', action='store_true', help="Skip fingerprinting assets.")
    parser.add_argument('--debug', action='store_true', help='Enable debug output')

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    if args.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    optimizer = Optimizer(
        source_dir=args.source_dir,
        destination_bucket=args.destination_bucket,
        exclude=args.exclude,
        skip_assets=args.skip_assets,
        output_dir=args.output_dir,
        aws_access_key_id=args.aws_access_key_id,
        aws_secret_access_key=args.aws_secret_access_key,
        skip_s3_upload=args.skip_s3_upload,
        region=args.region,
        domains=args.domains,
        prefix=args.prefix,
        gzip=args.gzip,
        skip_fingerprinting=args.skip_fingerprinting
    )

    optimizer.optimize()


if __name__ == '__main__':
    main()
