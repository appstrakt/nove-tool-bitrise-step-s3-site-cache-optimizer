from importlib_metadata import version

__version__ = version('s3-site-cache-optimizer')
